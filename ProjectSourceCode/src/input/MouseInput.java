package input;

import graphics.gui.Button;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import knight.Game;

public class MouseInput implements MouseMotionListener, MouseListener {

	private int x;
	private int y;

	public void mouseClicked(MouseEvent e) {

	}

	/**
	 * Tells where mouse is pressed, to evaluate which button is pressed.
	 * (Start/rank/info/Exit)
	 *
	 * @param MouseEvent e
	 */
	public void mousePressed(MouseEvent e) {
		for (int i = 0; i < Game.getLnch().getButtons().length; i++) {
			Button button = Game.getLnch().getButtons()[i];
			if (x >= button.getX() && y >= button.getY()
					&& x < button.getX() + button.getWidth()
					&& y < button.getY() + button.getHeight()) {
				button.TriggerEvent();
			}
		}
			Button buttonr = Game.getRnk().getButton();
			Button buttonInf = Game.getInfo().getbutton();
			Button buttonSel = Game.getSel().getButton1();
			Button buttonSel2 = Game.getSel().getButton2();
			
			if (x >= buttonr.getX() && y >= buttonr.getY()
					&& x < buttonr.getX() + buttonr.getWidth()
					&& y < buttonr.getY() + buttonr.getHeight()) {
				buttonr.TriggerEvent();
			}
			if (x >= buttonInf.getX() && y >= buttonInf.getY()
					&& x < buttonInf.getX() + buttonInf.getWidth()
					&& y < buttonInf.getY() + buttonInf.getHeight()) {
				buttonInf.TriggerEvent();
			}
			if (x >= buttonSel.getX() && y >= buttonSel.getY()
					&& x < buttonSel.getX() + buttonSel.getWidth()
					&& y < buttonSel.getY() + buttonSel.getHeight()) {
				buttonSel.TriggerEvent();
			}
			if (x >= buttonSel2.getX() && y >= buttonSel2.getY()
					&& x < buttonSel2.getX() + buttonSel2.getWidth()
					&& y < buttonSel2.getY() + buttonSel2.getHeight()) {
				buttonSel2.TriggerEvent();
			}
		
	}

	public void mouseReleased(MouseEvent e) {

	}

	public void mouseEntered(MouseEvent e) {

	}

	public void mouseExited(MouseEvent e) {

	}

	public void mouseDragged(MouseEvent e) {

	}

	/**
	 * Returns Mouse position X and Y
	 *
	 * @param MouseEvent e
	 */
	public void mouseMoved(MouseEvent e) {
		this.x = e.getX();
		this.y = e.getY();
	}
}
