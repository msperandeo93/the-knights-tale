package input;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import entity.EnergyWave;
import entity.Entity;
import knight.Game;
import knight.Id;

public class KeyInput implements KeyListener {
	
	/**
	 * Manages actions on key pressed.
	 *
	 * @param KeyEvent e
	 */
	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();
		for (int i = 0; i < Game.getHandler().listEntity().size(); i++) {
			Entity en = Game.getHandler().listEntity().get(i);
			if (en.getId() == Id.player) {
				switch (key) {
				case KeyEvent.VK_W: // jump
					if (!en.isJumping()) {
						en.setJump(true);
						en.setGravity(8.0);
					}
					break;
				case KeyEvent.VK_A: // left
					en.setVelX(-5);
					break;
				case KeyEvent.VK_D: // right
					en.setVelX(5);
					break;
				case KeyEvent.VK_K: // shoot
					if(!Game.getPgS()) {
						if (!en.isFire() && en.isPowerUpGet()) {
							en.setFire(true);
							if (en.isSx()) { // checks coordinates to create wave
								Entity waveSx = new EnergyWave(en.getX() - 70, en.getY(), 64, 64, true, Id.energyWave, Game.getHandler());
								waveSx.setDirection(0);
								Game.getHandler().addEntity(waveSx);
								Game.getPlyrAtck().play();
							} else if (en.isDx()) {
								EnergyWave waveDx = (new EnergyWave(en.getX() + 70, en.getY(), 64, 64, true, Id.energyWave, Game.getHandler()));
								waveDx.setDirection(1);
								Game.getHandler().addEntity(waveDx);
								Game.getPlyrAtck().play();
							}
						}
					}
					break;
				}
			}
		}
	}

	/**Manages key release, ending the precedently started action.
	 *
	 * @param KeyEvent
	 */
	public void keyReleased(KeyEvent e) {
		int key = e.getKeyCode();
		for (int i = 0; i < Game.getHandler().listEntity().size(); i++) {
			Entity en = Game.getHandler().listEntity().get(i);
			if (en.getId() == Id.player) {
				switch (key) {
				case KeyEvent.VK_W:
					en.setVelY(0);
					break;
				case KeyEvent.VK_A:
					en.setVelX(0);
					break;
				case KeyEvent.VK_D:
					en.setVelX(0);
					break;
				case KeyEvent.VK_K:
					en.setFire(false);
					break;
				}
			}
		}
	}

	public void keyTyped(KeyEvent arg0) {
		// not used
	}

}
