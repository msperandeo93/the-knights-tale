package graphics;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

public class SpriteSheet {

	private BufferedImage sheet;

	public SpriteSheet(String path) {
		try {
			this.sheet = ImageIO.read(getClass().getResource(path));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * From image on resources (presenting a virtual 32x32 grid) containing objects to draw,
	 * selects the chosen image.
	 * 
	 * @param x, y
	 * @return sheet.getSubimage(x*32-32, y*32-32, 32, 32) relative to
	 * 			sub-image selected with 32x32 dimensions.
	 */
	public BufferedImage getSprite(int x, int y) {
		return this.sheet.getSubimage(x * 32 - 32, y * 32 - 32, 32, 32);
	}

}
