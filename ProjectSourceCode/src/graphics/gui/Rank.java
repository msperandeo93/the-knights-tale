package graphics.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Map;
import java.util.TreeMap;

import javax.imageio.ImageIO;

import knight.Game;

public class Rank implements Serializable {
	
	private static final long serialVersionUID = 7526472295622776147L;

	private static final String FILE_NAME = "rank.txt";
	private BufferedImage image;
	
	private String[] names;
	private Integer[] scores;
	
	private Button button;
	private Map<Integer, String> m;
	private Map<Integer, String> dm;
	

	public Rank() throws IOException {
		this.button = new Button(700, 600, 300, 100, "Back");
		this.m = new TreeMap<Integer, String>();
		dm = new TreeMap<Integer, String>();
		dm.put(1, "aaa");
		dm.put(2, "bbb");
		dm.put(3, "ccc");
		dm.put(4, "ddd");
		dm.put(5, "eee");
		read();
		try {
			this.image = ImageIO.read(getClass().getResource("/launcher.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Draws rank screen to show players scores.
	 *
	 * @param Graphics g
	 */
	public void render(Graphics g) {
		g.drawImage(image, 0, 0, Game.getFrameWidth(), Game.getFrameHeight(), null);
		g.setColor(Color.white);

		
		
		names = m.values().toArray(new String[m.size()]);
		scores = m.keySet().toArray(new Integer[m.size()]);
		
		g.setFont(new Font("courier", Font.BOLD, 35));
		g.drawString("NOME", 100, 100);
		g.drawString("PUNTEGGIO", 650, 100);
		
		g.setFont(new Font("courier", Font.BOLD, 25));
		int j = 150;
		int h = 1;
		for(int i=names.length-1; i>names.length-6; i--) {
			g.drawString( h + ". " + names[i].toString(), 100, j );
			g.drawString(scores[i].toString(), 650, j );
			j+=50;
			h++;
		}
		
		//g.drawString(m.values().toString(), 100, 100);
		//g.drawString(m.keySet().toString(), 100, 200);
		this.button.render(g);
	}

	/**
	 * Writes on file to save players scores.
	 *
	 * @param TreeMap<Integer, String> map
	 */
	public void write(Map<Integer, String> map) {
		// Writes on file
		try {
			File f = new File(FILE_NAME);
			FileOutputStream fos = new FileOutputStream(f);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(map);
			oos.flush();
			oos.close();
			fos.close();
		} catch (Exception e) {
		}
	}

	/**
	 * Reads the file with players scores.
	 *
	 */
	public void read() {
		// Reads from file
		TreeMap<Integer, String> mapInFile = new TreeMap<Integer, String>();
		try {
			File f = new File(FILE_NAME);
			FileInputStream fis = new FileInputStream(f);
			ObjectInputStream ois = new ObjectInputStream(fis);
			
			@SuppressWarnings("unchecked")
			TreeMap<Integer, String> readObject = (TreeMap<Integer, String>) ois.readObject();
			mapInFile = readObject;
			ois.close();
			fis.close();
		} catch (Exception e) {
		}
		m.putAll(mapInFile);
		if(m.isEmpty()){
			m.putAll(dm);
		}
	}
	
	/**Returns Button back
	 * 
	 * @return
	 */
	
	public Button getButton(){
		return button;
	}
	
	/** Returns the map which contains all rankings
	 * 
	 * @return Map<Integer, String>
	 */
	
	public Map<Integer, String> getMap (){
		return m;
	}
}