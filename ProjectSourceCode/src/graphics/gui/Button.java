package graphics.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;

import javax.swing.JOptionPane;

import knight.Game;

public class Button {
	private int x;
	private int y;
	private int width;
	private int height;
	private String label;
	

	public Button(int x, int y, int width, int height, String label) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.label = label;
	}

	/**
	 * Draws the button and writes in his center
	 * 
	 *
	 * @param Graphics g
	 */
	public void render(Graphics g) {
		g.setColor(Color.WHITE);
		g.setFont(new Font("Phosphate", Font.BOLD, 50));
		g.drawRect(getX(), getY(), getWidth(), getHeight());

		// Writes in button's center
		FontMetrics fm = g.getFontMetrics();
		int stringX = (getWidth() - fm.stringWidth(getLabel())) / 2;
		int stringY = (fm.getAscent() + (getHeight() - (fm.getAscent() + fm
				.getDescent())) / 2);
		g.drawString(getLabel(), getX() + stringX, getY() + stringY);
	}

	/**
	 * According to selected button's content, exits or starts the game.
	 * 
	 *
	 */
	public void TriggerEvent() {
		if (getLabel().toLowerCase().contains("start")) {	
			Game.setSelB(true);
		} else if (getLabel().toLowerCase().contains("ranking")) {
			Game.setRnkB(true);
		} else if (getLabel().toLowerCase().contains("info")) {
			Game.setInfB(true);
		} else if (getLabel().toLowerCase().contains("exit")) {
			Game.getRnk().write(Game.getRnk().getMap());
			System.exit(0);
		}
		else if (getLabel().toLowerCase().contains("back")){
			Game.setRnkB(false);
			Game.setInfB(false);
		}
		else if (getLabel().toLowerCase().contains("zen kyu")){
			Game.setNm(JOptionPane.showInputDialog("Inserisci il nome che verrà salvato nella classifica:"));
			Game.setPlng(true);
		}
		else if(getLabel().toLowerCase().contentEquals("mairo")){
			Game.setNm(JOptionPane.showInputDialog("Inserisci il nome che verrà salvato nella classifica:"));
			Game.setPgS();
			Game.setDthScrn(true);
			Game.setPlng(true);
		}
		
	}
	
	public void SetLabel(String s){
		this.label = s;
	}

	/**
	 * Returns X value.
	 *
	 * @return this.x
	 */
	public int getX() {
		return x;
	}
	
	/**
	 * Returns Y value.
	 *
	 * @return this.y
	 */
	public int getY() {
		return y;
	}

	/**
	 * Returns width.
	 *
	 * @return this.width
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * Returns height.
	 *
	 * @return this.height
	 */
	public int getHeight() {
		return height;
	}
	
	/**
	 * Returns label.
	 *
	 * @return this.label
	 */
	public String getLabel() {
		return label;
	}
}
