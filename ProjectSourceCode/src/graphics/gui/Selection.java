package graphics.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;


import knight.Game;



public class Selection {

	private BufferedImage image;
	
	private Button button1;
	private Button button2;
	public Selection(){
		this.button1 = new Button(100, 300, 300, 100, "Zen Kyu");
		this.button2 = new Button(650, 300, 300, 100, "Mairo");
		
		try {
			this.image = ImageIO.read(getClass().getResource("/launcher.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}
	
	public void render(Graphics g){
		g.drawImage(image, 0, 0, Game.getFrameWidth(), Game.getFrameHeight(), null);
		g.setColor(Color.black);
		
		g.drawImage(Game.getPlDx1().getBufferedImage(), 200, 300, 100, 100, null);
		g.drawImage(Game.getPl2Dx1().getBufferedImage(), 750, 300, 100, 100, null);
		
		this.button1.render(g);
		this.button2.render(g);
	}
	
	public Button getButton1(){
		return button1;
	}
	
	public Button getButton2(){
		return button2;
	}
}
