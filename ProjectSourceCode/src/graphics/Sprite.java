package graphics;

import java.awt.image.BufferedImage;

public class Sprite {

	private BufferedImage image;

	public Sprite(SpriteSheet sheet, int x, int y) {
		this.image = sheet.getSprite(x, y);
	}

	/**
	 * Returns the image from geSprite method, which gets the image from a 32x32 grid containing objects.
	 * 
	 *
	 * @return image
	 */
	public BufferedImage getBufferedImage() {
		return this.image;
	}
}
