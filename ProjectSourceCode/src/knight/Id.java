package knight;

/**
 * Contains the Ids of all entities within the game.
 */
public enum Id {
	player, firstGhost, secondGhost, thirdGhost, firstBoss, secondBoss, fllRock, gun, slug, wall, ground, powerUp, energyWave, energyWaveBoss, crystal, specialCystal, door, crown;																																				// boss
}
