package knight;

import entity.Entity;
import knight.Game;

public class Camera {

	private int x;
	private int y;

	/**
	 * Manages camera movement according to player position.
	 *
	 * @param Entity player
	 */
	public void tick(Entity player) {
		setX(-player.getX() + Game.getwdth() * 2);
		setY(-player.getY() + Game.getHgth() * 2);
	}

	/**
	 * Sets X value.
	 *
	 * @param x
	 */
	private void setX(int x) {
		this.x = x;
	}

	/**
	 * Sets Y value.
	 *
	 * @param Y
	 */
	private void setY(int y) {
		this.y = y;
	}
	
	/**
	 * Returns X position.
	 * 
	 * @return this.x
	 */
	public int getX() {
		return x;
	}
	
	/**
	 * Returns Y position.
	 * 
	 * @return this.y
	 */
	public int getY() {
		return y;
	}
}
