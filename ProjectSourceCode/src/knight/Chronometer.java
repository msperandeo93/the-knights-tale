package knight;

public class Chronometer {

	private long startTime;
	private long stopTime;
	private long totalTime;
	private double timeScore;

	public Chronometer(){
		
	}
	
	/**
	 * Sets start time.
	 */
	public void startTime() {
		startTime = System.currentTimeMillis();
	}

	/**
	 * Sets end time.
	 */
	public void stopTime() {
		stopTime = System.currentTimeMillis();
		setTime();
	}
	
	/**
	 * Sets global time played.
	 */
	private void setTime() {
		this.totalTime = (stopTime - startTime) / 1000;
		timeScore();
	}
	
	/**
	 * Sets the influence of time in the score.
	 */
	private double timeScore() {
		if (totalTime < 100) {
			return timeScore = 1.5;
		} else if (totalTime < 150) {
			return timeScore = 1.3;
		} else {
			return timeScore = 1;
		}
	}
	
	public long getTotTime(){
		return this.totalTime;
	}
	
	public double getTimeScore(){
		return this.timeScore;
	}
	
	public long getStartTime(){
		return this.startTime;
	}
}
