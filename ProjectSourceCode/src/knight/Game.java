package knight;
import java.awt.image.*;
import java.io.IOException;
import java.awt.*;

import javax.imageio.ImageIO;
import javax.swing.*;

import input.*;
import entity.Entity;
import graphics.*;
import graphics.gui.*;

public class Game extends Canvas implements Runnable {

	private static final long serialVersionUID = 7526472295622776147L;
	
	private static final int WIDTH = 270;
	private static final int HEIGHT = WIDTH/14*10;
	private static final int SCALE = 4;

	private Thread thread;
	private boolean running = false;
	
	private static boolean PgS = false;
	
	private static int deathScreenTime=0;
	private static BufferedImage[] levels;

	private static double totalPoints=0;
	private static int crystalGet=0;
	private static int totCrystalGet=0;
	private static int firstGhostKill=0;
	private static int thirdGhostKill=0;
	private static int totGhostKill=0;
	private static int boss_1Kill=0;
	private static int boss_2Kill=0;
	private static int lives=3;
	private static int level=0;
	
	//
	//	NB
	//	MODIFICARE IN PRIVATE
	//	E CLASSI DI CONSEGUENZA
	//
	private static String name;
	private static int frames=0;
	private static int ticks=0;

	private static boolean showDeathScreen=true;
	private static boolean gameOver=false;
	private static boolean winner=false;
	private static boolean beginGame=true;
	private static boolean playing=false;
	private static boolean rankb=false;
	private static boolean infob=false;
	private static boolean selb=false;

	private static Handler handler;
	private static SpriteSheet sheet;
	private static SpriteSheet sheet2;
	private static Camera cam;
	private static Launcher launcher;
	private static Rank rank;
	private static Info info;
	private static MouseInput mouse;
	private static KeyInput keyBoard;
	private static Chronometer chrono;
	private static Selection selection;
	
	private static Sprite p2s_1;
	private static Sprite p2s_2;
	private static Sprite p2d_1;
	private static Sprite p2d_2;
	private static Sprite p2js;
	private static Sprite p2jd;

	

	private static Sprite playerSx_1;
	private static Sprite playerSx_2;
	private static Sprite playerDx_1;
	private static Sprite playerDx_2;
	private static Sprite playerJumpDx;
	private static Sprite playerJumpSx;
	private static Sprite playerFireDx;
	private static Sprite playerFireSx;
	private static Sprite ground;
	private static Sprite wall;
	private static Sprite powerUp;
	private static Sprite slug;
	private static Sprite energyWave;
	private static Sprite energyWaveBossDx;
	private static Sprite energyWaveBossSx;
	private static Sprite gun;
	private static Sprite fllRock;
	private static Sprite firstGhostDx;
	private static Sprite firstGhostSx;
	private static Sprite secondGhostDx;
	private static Sprite secondGhostSx;
	private static Sprite thirdGhostDx;
	private static Sprite thirdGhostSx;
	private static Sprite firstBossDx;
	private static Sprite firstBossSx;
	private static Sprite secondBossDx;
	private static Sprite secondBossSx;
	private static Sprite door;
	private static Sprite crown;
	private static Sprite crystal;
	private static Sprite specialCrystal;
	
	private static Sound themeTone2;
	private static Sound themeTone;
	private static Sound waveHit;
	private static Sound explode;
	private static Sound playerHit;
	private static Sound playerAttack;
	private static Sound getPowerUp;
	private static Sound gameOverTone;
	private static Sound levelUpTone;
	private static Sound getObj;
	private static Sound winnerTone;


	public Game() {
		Dimension size = new Dimension(WIDTH*SCALE,HEIGHT*SCALE);
		setPreferredSize(size);
		setMaximumSize(size);
		setMinimumSize(size);
	}

	/**
	 * Initializes everything you can see in the game.
	 *
	 */
	private void init() throws IOException {
		handler = new Handler();
		sheet = new SpriteSheet("/spritesheet.png");
		sheet2 = new SpriteSheet("/Mario.png");
		cam = new Camera();
		launcher = new Launcher();
		chrono = new Chronometer();
		rank = new Rank();
		info = new Info();
		selection = new Selection();
		mouse = new MouseInput();
		keyBoard = new KeyInput();
		addKeyListener(keyBoard);
		addMouseListener(mouse);
		addMouseMotionListener(mouse);
		
		p2s_1 = new Sprite(sheet2, 6, 1);
		p2s_2 = new Sprite(sheet2, 5, 1);
		p2d_1 = new Sprite(sheet2, 2, 1);
		p2d_2 = new Sprite(sheet2, 3, 1);
		p2js = new Sprite(sheet2, 4, 1);
		p2jd = new Sprite(sheet2, 1, 1);

		playerDx_1 = new Sprite(sheet, 5, 1);
		playerDx_2 = new Sprite(sheet, 6, 1);
		playerSx_1 = new Sprite(sheet, 2, 1);
		playerSx_2 = new Sprite(sheet, 1, 1);
		playerJumpDx = new Sprite(sheet, 7, 1);
		playerJumpSx = new Sprite(sheet, 3, 1);
		playerFireDx = new Sprite(sheet, 8, 1);
		playerFireSx = new Sprite(sheet, 4, 1);
		ground = new Sprite(sheet, 4, 3);
		wall = new Sprite(sheet, 3, 3);
		powerUp = new Sprite(sheet, 7, 3);
		fllRock = new Sprite(sheet, 12, 3);
		slug = new Sprite(sheet, 16, 3);
		energyWave = new Sprite(sheet, 6, 3);
		energyWaveBossDx = new Sprite(sheet, 3, 8);
		energyWaveBossSx = new Sprite(sheet, 4, 8);
		door = new Sprite(sheet, 1, 3);
		crown = new Sprite(sheet, 8, 3);
		crystal = new Sprite(sheet, 5, 3);
		specialCrystal = new Sprite(sheet, 9, 3);
		gun = new Sprite(sheet, 14, 3);
		firstGhostDx = new Sprite(sheet, 3, 6);
		firstGhostSx = new Sprite(sheet, 2, 6);
		secondGhostDx = new Sprite(sheet, 5, 6);
		secondGhostSx = new Sprite(sheet, 6, 6);
		thirdGhostDx = new Sprite(sheet, 9, 6);
		thirdGhostSx = new Sprite(sheet, 8, 6);
		firstBossDx = new Sprite(sheet, 1, 8);
		firstBossSx = new Sprite(sheet, 2, 8);
		secondBossDx = new Sprite(sheet, 8, 8);
		secondBossSx = new Sprite(sheet, 7, 8);

		levels = new BufferedImage[3];

		try {
			levels[0] = ImageIO.read(getClass().getResource("/lev_1.png"));
			levels[1] = ImageIO.read(getClass().getResource("/lev_2.png"));
			levels[2] = ImageIO.read(getClass().getResource("/lev_3.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		Game.themeTone2 = new Sound("/audio/MarioTone.wav");
		Game.themeTone = new Sound("/audio/themeTone.wav");
		Game.waveHit = new Sound("/audio/wavehit.wav");
		Game.playerHit = new Sound("/audio/playerhit.wav");
		Game.playerAttack = new Sound("/audio/playerattack.wav");
		Game.explode = new Sound("/audio/explode.wav");
		Game.getPowerUp = new Sound("/audio/powerup.wav");
		Game.gameOverTone = new Sound("/audio/gameOverTone.wav");
		Game.levelUpTone = new Sound("/audio/levelUpTone.wav");
		Game.getObj = new Sound("/audio/coinTone.wav");
		Game.winnerTone = new Sound("/audio/winnerTone.wav");
	}

	/**
	 * Manages start of the Thread.
	 *
	 */
	private synchronized void start() {
		System.out.println("Partito!");
		if (running){
			return;
		}
		running = true;
		thread = new Thread(this, "Thread");
		thread.start();
	}

	/**
	 * Manages stop of the Thread.
	 *
	 */
	private synchronized void stop() {
		if (!running){
			return;
		}
		running = false;
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void run() {
		try {
			init();
		} catch (IOException e) {
			e.printStackTrace();
		}
		requestFocus();
		long lastTime = System.nanoTime();
		long timer = System.currentTimeMillis();
		double delta = 0;
		double ns = 1000000000.0 / 60.0;
		setFrames(0);
		setTicks(0);
		while (running) {
			long now = System.nanoTime();
			delta += (now - lastTime) / ns;
			lastTime = now;
			while (delta >= 1) {
				tick();
				setTicks(getTicks() + 1);
				delta--;
			}
			render();
			setFrames(getFrames() + 1);
			if (System.currentTimeMillis() - timer > 1000) {
				timer += 1000;
				setFrames(0);
				setTicks(0);
			}
		}
		stop();
	}

	/**
	 * Given a screen, creates DeathScreen, GameOver or Win contents, according to situation.
	 *
	 */
	public void render() {
		BufferStrategy bs = getBufferStrategy();
		if (bs == null) {
			createBufferStrategy(3);
			return;
		}
		Graphics g = bs.getDrawGraphics();
		Graphics g2 = bs.getDrawGraphics();
		g.setColor(Color.black);
		g.fillRect(0, 0, getWidth(), getHeight());
		if (!showDeathScreen) {
			g.setColor(Color.white);
			g.setFont(new Font("courier", Font.BOLD, 20));
			g.drawImage(Game.crystal.getBufferedImage(), 20, 20, 40, 40, null);
			g.drawString("X" + crystalGet + "/" + handler.getTotCryst(), 60, 55);
			g.drawImage(Game.firstGhostDx.getBufferedImage(), 150, 20, 40, 40, null);
			g.drawString("X" + firstGhostKill + "/" + handler.getTotFirstGh(), 190, 55);
			g.drawImage(Game.secondGhostDx.getBufferedImage(), 260, 20, 40, 40, null);
			g.drawString("X" + handler.getTotSecGh(), 300, 55);
			g.drawImage(Game.thirdGhostDx.getBufferedImage(), 360, 20, 40, 40, null);
			g.drawString("X" + thirdGhostKill + "/" + handler.getTotThrdGh(), 400, 55);
		}
		if (showDeathScreen) {
			if (winner) {
				g.setColor(Color.green);
				if(PgS) {
					g.drawImage(Game.p2d_1.getBufferedImage(), 200, 300, 100, 100, null);
				} else {
					g.drawImage(Game.playerDx_1.getBufferedImage(), 200, 300, 100, 100, null);
				}
				
				g.setFont(new Font("courier", Font.BOLD, 90));
				g.drawString("You Win!", 350, 400);
				if(PgS) {
					g.drawImage(Game.p2s_1.getBufferedImage(), 800, 300, 100, 100, null);
				} else {
					g.drawImage(Game.playerSx_1.getBufferedImage(), 800, 300, 100, 100, null);
				}
				
				g2.setColor(Color.green);
				g2.setFont(new Font("courier", Font.BOLD, 30));
				g2.drawString("Tempo di gioco: "+chrono.getTotTime()+" secondi", 290, 470);
				g2.drawString("Nemici sconfitti: "+totGhostKill, 290, 520);
				g2.drawString("Cristalli raccolti: "+totCrystalGet, 290, 570);
				g2.drawString("Punteggio totale realizzato: "+Game.totalPoints, 290, 620);
			} else if (!gameOver) {
				g.setColor(Color.white);
				if(PgS) {
					g.drawImage(Game.p2d_1.getBufferedImage(), 420, 300, 100, 100, null);
				} else {
					g.drawImage(Game.playerDx_1.getBufferedImage(), 420, 300, 100, 100, null);
				}
				
				g.setFont(new Font("courier", Font.BOLD, 70));
				g.drawString("X" + lives, 520, 400);
			} else {
				g.setColor(Color.red);
				g.drawImage(Game.secondGhostDx.getBufferedImage(), 190, 300, 100, 100, null);
				g.setFont(new Font("courier", Font.BOLD, 90));
				g.drawString("Game Over!", 290, 400);
				g.drawImage(Game.thirdGhostSx.getBufferedImage(), 810, 300, 100, 100, null);
				g2.setColor(Color.red);
				g2.setFont(new Font("courier", Font.BOLD, 30));
				g2.drawString("Tempo di gioco: "+chrono.getTotTime()+" secondi", 290, 470);
				g2.drawString("Nemici sconfitti: "+totGhostKill, 290, 520);
				g2.drawString("Cristalli raccolti: "+totCrystalGet, 290, 570);
				g2.drawString("Punteggio totale realizzato: "+Game.totalPoints, 290, 620);
			}
		}
		if (playing){
			g.translate(cam.getX(), cam.getY());
		}
		if (!showDeathScreen && playing){
			handler.render(g);
		} else if (!playing) {
			if (!rankb || !infob) {
				launcher.render(g);
			}
			if (rankb) {
				rank.render(g);
			} else if (infob) {
				info.render(g, g2);
			}
			if (selb) {
				selection.render(g);
			}
		}
		g.dispose();
		bs.show();
	}

	/**
	 * Manages what happens within the game. For example creation of levels.
	 *
	 */
	public void tick() {
		if (playing){
			handler.tick();
		}
		for (int i = 0; i < handler.listEntity().size(); i++) {
			Entity e = handler.listEntity().get(i);
			if (e.getId() == Id.player) {
				cam.tick(e);
			}
		}
		if (showDeathScreen){
			themeTone2.stop();
			themeTone.stop();
			deathScreenTime++;
		}	
		if (deathScreenTime >= 120 && !gameOver) {
			showDeathScreen = false;
			deathScreenTime = 0;
			handler.clearLevel();
			handler.createLevel(levels[level]);
			if(!PgS)
				Game.themeTone.play();
			else 
				Game.themeTone2.play();
			if (beginGame){
				chrono.startTime();
			}
		}
		if (deathScreenTime >= 240 && (gameOver || winner)) {
			chrono.stopTime();
			themeTone2.stop();
			deathScreenTime = 0;
			gameOver = false;
			winner = false;
			beginGame = true;
			Game.playing = false;
			Game.selb = false;
			Game.PgS = false;
			Game game = new Game();
			lives = 3;
			level = 0;
			game.start();
		}
	}

	/**
	 * Manages the switch of level when door is reached.
	 *
	 */
	public static void switchLevel() {
		Game.level++;
		totGhostKill = totGhostKill+(firstGhostKill+thirdGhostKill);
		totCrystalGet = totCrystalGet+crystalGet;
		handler.setTotCry(true); // Resets total of objects created within the level.
		handler.setTotFirstGh(true);
		handler.setTotSecGh(true);
		handler.setTotThrdGh(true);
		firstGhostKill = 0;
		thirdGhostKill = 0;
		crystalGet = 0;
		handler.clearLevel();
		handler.createLevel(levels[level]);
		Game.themeTone2.stop();
		Game.themeTone.stop();
		Game.levelUpTone.play();
		if(!PgS)
			Game.themeTone.play();
		else
			Game.themeTone2.play();
	}

	public static void setTotPoints(double n){
		totalPoints = n;
	}
	
	public static void setCrstlGet(int n){
		if(n==0){
			crystalGet=0;
		}
		crystalGet += n;
	}
	
	public static void setFrstGhKll(int n){
		if(n==0){
			firstGhostKill=0;
		}
		firstGhostKill += n;
	}
	
	public static void setThGhKll(int n){
		if(n==0){
			thirdGhostKill=0;
		}
		thirdGhostKill += n;
	}
	
	public static void setBoss1Kll(int n){
		boss_1Kill = 1;
	}
	
	public static void setBoss2Kll(int n){
		boss_2Kill = 1;
	}
	
	public static void setLives(int n){
		lives += n;
	}
	
	//settare stringa name
	public static void setNm(String s){
		name = s;
	}
	
	public static void setTicks(int ticks) {
		Game.ticks = ticks;
	}

	public static void setFrames(int frames) {
		Game.frames = frames;
	}
	
	public static void setDthScrn(boolean b){
		showDeathScreen = b;
	}
	
	public static void setGmOvr(boolean b){
		gameOver = b;
	}
	
	public static void setWnnr(boolean b){
		winner = b;
	}
	
	public static void setBgnGm(boolean b){
		beginGame = b;
	}
	
	public static void setPlng(boolean b){
		playing = b;
	}
	
	/**Methods to manage GUI changes
	 * 
	 * @param b
	 */
	public static void setRnkB(boolean b){
		rankb = b;
	}
	
	public static void setInfB(boolean b){
		infob = b;
	}
	
	public static void setSelB(boolean b){
		selb = b;
	}
	
	
	
	public static Sprite getFllRock() {
		return fllRock;
	}
	
	public static Sprite getSlug() {
		return slug;
	}

	public static Sprite getGun() {
		return gun;
	}
	
	public static int getFrames() {
		return frames;
	}
	
	public static int getTicks() {
		return ticks;
	}
	
	public static String getNm(){
		return name;
	}
	
	public static int getLives(){
		return lives;
	}
	
	public static int getBoss2Kll(){
		return boss_2Kill;
	}
	
	public static int getBoss1Kll(){
		return boss_1Kill;
	}
	
	public static int getTotGhKll(){
		return totGhostKill;
	}
	
	public static int getTotCrstlGet(){
		return totCrystalGet;
	}
	
	public static double getTotPoints(){
		return totalPoints;
	}
	
	/**
	 * Gives window's width.
	 *
	 * @return width*scale
	 */
	public static int getFrameWidth() {
		return WIDTH * SCALE;
	}

	/**
	 * Gives window's height.
	 *
	 * @return height*scale
	 */
	public static int getFrameHeight() {
		return HEIGHT * SCALE;
	}
	
	public static int getHgth() {
		return HEIGHT;
	}
	
	public static int getwdth() {
		return WIDTH;
	}
	
	public static int getScale() {
		return SCALE;
	}

	/**
	 * Gives visible area within the game.
	 *
	 * @return Rectangle() or null 
	 */
	public static Rectangle getVisibleArea() {
		for (int i = 0; i < handler.listEntity().size(); i++) {
			Entity e = handler.listEntity().get(i);
			if (e.getId() == Id.player)
				return new Rectangle(e.getX() - (getFrameWidth() / 2 - 5),
						e.getY() - (getFrameHeight() / 2 - 5),
						getFrameWidth() + 10, getFrameHeight() + 10);
		}
		return null;
	}
	
	public static Handler getHandler(){
		return handler;
	}
	
	public static SpriteSheet getSheet(){
		return sheet;
	}
	
	public static SpriteSheet getSheet2() {
		return sheet2;
	}

	public static Camera getCam(){
		return cam;
	}
	
	public static Launcher getLnch(){
		return launcher;
	}
	
	public static Rank getRnk(){
		return rank;
	}
	
	public static Info getInfo(){
		return info;
	}
	
	public static Selection getSel(){
		return selection;
	}
	
	public static MouseInput getMsInp(){
		return mouse;
	}

	public static KeyInput getKyInp(){
		return keyBoard;
	}
	
	public static Chronometer getChrn(){
		return chrono;
	}
	
	public static Sound getThmTn(){
		return themeTone;
	}

	public static Sound getWvHit(){
		return waveHit;
	}
	
	public static Sound getExpld(){
		return explode;
	}
	
	public static Sound getPlyrHit(){
		return playerHit;
	}
	
	public static Sound getPlyrAtck(){
		return playerAttack;
	}
	
	public static Sound getPwrdUp(){
		return getPowerUp;
	}

	public static Sound getGmOvTn(){
		return gameOverTone;
	}

	public static Sound getLvlUpTn(){
		return levelUpTone;
	}

	public static Sound getObj(){
		return getObj;
	}

	public static Sound getWnnTn(){
		return winnerTone;
	}
	
	public static Sprite getPlSx1(){
		return playerSx_1;
	}

	public static Sprite getPlSx2(){
		return playerSx_2;
	}

	public static Sprite getPlDx1(){
		return playerDx_1;
	}

	public static Sprite getPlDx2(){
		return playerDx_2;
	}
	
	public static Sprite getPlJmpDx(){
		return playerJumpDx;
	}
	
	public static Sprite getPlJmpSx(){
		return playerJumpSx;
	}
	
	public static Sprite getPlFrDx(){
		return playerFireDx;
	}
	
	public static Sprite getPlFrSx(){
		return playerFireSx;
	}
	
	public static Sprite getPl2Sx1() {
		return p2s_1;
	}
	
	public static Sprite getPl2Sx2() {
		return p2s_2;
	}
	
	public static Sprite getPl2Dx1() {
		return p2d_1;
	}
	
	public static Sprite getPl2Dx2() {
		return p2d_2;
	}
	
	public static Sprite getPl2JmpS() {
		return p2js;
	}
	
	public static Sprite getPl2JmpD() {
		return p2jd;
	}
	
	public static Sprite getGrnd(){
		return ground;
	}

	public static Sprite getWll(){
		return wall;
	}
	
	public static Sprite getPwUp(){
		return powerUp;
	}

	public static Sprite getEnrgWv(){
		return energyWave;
	}

	public static Sprite getEnrgWvBssDx(){
		return energyWaveBossDx;
	}
	
	public static Sprite getEnrgWvBssSx(){
		return energyWaveBossSx;
	}

	public static Sprite getFrstGhDx(){
		return firstGhostDx;
	}
	
	public static Sprite getFrstGhSx(){
		return firstGhostSx;
	}

	public static Sprite getScnGhDx(){
		return secondGhostDx;
	}

	public static Sprite getScnGhSx(){
		return secondGhostSx;
	}

	public static Sprite getThGhDx(){
		return thirdGhostDx;
	}
	
	public static Sprite getThGhSx(){
		return thirdGhostSx;
	}
	
	public static Sprite getFrsBssDx(){
		return firstBossDx;
	}
	
	public static Sprite getFrsBssSx(){
		return firstBossSx;
	}
	
	public static Sprite getScnBssDx(){
		return secondBossDx;
	}
	
	public static Sprite getScnBssSx(){
		return secondBossSx;
	}

	public static Sprite getDoor(){
		return door;
	}
	
	public static Sprite getCrwn(){
		return crown;
	}

	public static Sprite getCrst(){
		return crystal;
	}
	
	public static Sprite getSpCrst(){
		return specialCrystal;
	}	
	
	public static boolean getPgS() {
		return PgS;
	}
	
	public static void setPgS() {
		PgS = true;
	}
	
	public static void main(String[] args) {
		Game game = new Game();
		JFrame frame = new JFrame("The knight's tale");
		frame.add(game);
		frame.pack();
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		game.start();
	}
}