package knight;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.LinkedList;

import tile.*;
import tile.map.*;
import tile.obj.*;
import entity.*;
import entity.characters.*;

public class Handler {

	private LinkedList<Entity> entity = new LinkedList<Entity>();
	private LinkedList<Tile> tile = new LinkedList<Tile>();
	private int totCrystal; 
	private int totFirstGhost;
	private int totSecondGhost;
	private int totThirdGhost;

	public Handler(){
		
	}
	
	/** 
	 * Draws just level's visible area. 
	 *
	 * @param Graphics g
	 */
	public void render(Graphics g) {
		for (int i = 0; i < entity.size(); i++) {
			Entity e = entity.get(i);
			if (Game.getVisibleArea()!=null	&& e.getBounds().intersects(Game.getVisibleArea())){
				e.render(g);
			}
		}
		for (int i = 0; i < tile.size(); i++) {
			Tile t = tile.get(i);
			if (Game.getVisibleArea()!=null	&& t.getBounds().intersects(Game.getVisibleArea())){
				t.render(g);
			}
		}
	}

	/**
	 * Manages the area.
	 *
	 */
	public void tick() {
		for (int i = 0; i < entity.size(); i++) {
			Entity e = entity.get(i);
			e.tick();
		}
		for (int i = 0; i < tile.size(); i++) {
			Tile t = tile.get(i);
			if (Game.getVisibleArea()!=null && t.getBounds().intersects(Game.getVisibleArea())){
				t.tick();
			}
		}
	}

	/**
	 * Adds the specified entity.
	 *
	 * @param Entity e
	 */
	public void addEntity(Entity e) {
		entity.add(e);
	}

	/**
	 * Removes the specified entity.
	 *
	 * @param Entity e
	 */
	public void removeEntity(Entity e) {
		entity.remove(e);
	}

	/**
	 * Adds the specified tile.
	 *
	 * @param Tile t
	 */
	public void addTile(Tile t) {
		tile.add(t);
	}

	/**
	 * Removes the specified tile.
	 *
	 * @param Tile t
	 */
	public void removetile(Tile t) {
		tile.remove(t);
	}

	/**
	 * Draws the level. Everything is done loading and analyzing the level image from a file. 
	 * For example, where pixels are black a wall is created; where pixel is red a power up is created,
	 * and so on with all entities.
	 * 
	 * @param BufferedImage level
	 */
	public void createLevel(BufferedImage level) {
		int width = level.getWidth();
		int height = level.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				int pixel = level.getRGB(x, y);
				int red = (pixel >> 16) & 0xff;
				int green = (pixel >> 8) & 0xff;
				int blue = (pixel) & 0xff;
				if (red == 0 && green == 0 && blue == 0){
					addTile(new Ground(x * 64, y * 64, 64, 64, true, Id.ground, this));
				}
				if (red == 64 && green == 64 && blue == 64){
					addTile(new Wall(x * 64, y * 64, 64, 64, true, Id.wall, this));
				}	
				if (red == 255 && green == 216 && blue == 0){
					if(Game.getPgS()) {
						addEntity(new Player2(x * 64, y * 64, 64, 64, false, Id.player, this));
					} else {
						addEntity(new Player(x * 64, y * 64, 64, 64, false, Id.player, this));
					}
				}	
				if (red == 0 && green == 255 && blue == 255){
					addEntity(new PowerUp(x * 64, y * 64, 64, 64, true, Id.powerUp, this));
				}	
				if (red == 255 && green == 0 && blue == 0) {
					addEntity(new FirstGhost(x * 64, y * 64, 64, 64, true, 	Id.firstGhost, this));
					totFirstGhost++;
				}
				if (red == 255 && green == 106 && blue == 0) {
					addEntity(new SecondGhost(x * 64, y * 64, 64, 64, true, Id.secondGhost, this));
					totSecondGhost++;
				}
				if (red == 127 && green == 0 && blue == 0) {
					addEntity(new ThirdGhost(x * 64, y * 64, 64, 64, true, Id.thirdGhost, this));
					totThirdGhost++;
				}
				if (red == 178 && green == 0 && blue == 255) {
					addEntity(new FirstBoss(x * 64, y * 64, 64, 64, true, Id.firstBoss, this));
				}
				if (red == 255 && green == 0 && blue == 220){
					addEntity(new SecondBoss(x * 64, y * 64, 64, 64, true, Id.secondBoss, this));
				}	
				if (red == 0 && green == 38 && blue == 255) {
					addTile(new Crystal(x * 64, y * 64, 64, 64, true, Id.crystal, this));
					totCrystal++;
				}
				if (red == 0 && green == 255 && blue == 33){
					addTile(new Door(x * 64, y * 64, 64, 64, true, Id.door,	this));
				}	
				if (red == 76 && green == 255 && blue == 0){
					addTile(new Crown(x * 64, y * 64, 64, 64, true, Id.crown, this));
				}
				if (red == 128 && green == 64 && blue == 0){
					//creatura che cade
					addEntity(new FallingRock(x * 64, y * 64, 64, 64, true, Id.fllRock, this));
				}
				if (red == 255 && green == 128 && blue == 255){
					addEntity(new Gun(x * 64, y * 64, 64, 64, true, Id.gun, this));
				}
			}
		}
	}

	/**
	 * Clears the level deleting everything.
	 *
	 */
	public void clearLevel() {
		entity.clear();
		tile.clear();
	}
	
	public LinkedList<Entity> listEntity(){
		return this.entity;
	}
	
	public LinkedList<Tile> listTile(){
		return this.tile;
	}
	
	public void setTotCry(boolean reset){
		if(reset){
			this.totCrystal = 0;
		}
		this.totCrystal++;
	}
	
	public void setTotFirstGh(boolean reset){
		if(reset){
			this.totFirstGhost = 0;
		}
		this.totFirstGhost++;
	}
	
	public void setTotSecGh(boolean reset){
		if(reset){
			this.totSecondGhost = 0;
		}
		this.totSecondGhost++;
	}
	
	public void setTotThrdGh(boolean reset){
		if(reset){
			this.totThirdGhost = 0;
		}
		this.totThirdGhost++;
	}
	
	public int getTotCryst(){
		return this.totCrystal;
	}
	
	public int getTotFirstGh(){
		return this.totFirstGhost;
	}
	
	public int getTotSecGh(){
		return this.totSecondGhost;
	}
	
	public int getTotThrdGh(){
		return this.totThirdGhost;
	}
}