package tile.map;

import java.awt.Graphics;

import tile.Tile;
import knight.Game;
import knight.Handler;
import knight.Id;

public class Ground extends Tile {

	public Ground(int x, int y, int width, int height, boolean solid, Id id, Handler handler) {
		super(x, y, width, height, solid, id, handler);

	}

	/**
	 * Draws the floor tile loading relative image.
	 * 
	 * @param Graphics g
	 */
	public void render(Graphics g) {
		g.drawImage(Game.getGrnd().getBufferedImage(), getX(), getY(), getWidth(), getHeight(), null);
	}

	public void tick() {
		// Not used as floor does nothing.
	}

}
