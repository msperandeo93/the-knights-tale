package tile.map;

import java.awt.Graphics;

import tile.Tile;
import knight.Game;
import knight.Handler;
import knight.Id;

public class Wall extends Tile {

	public Wall(int x, int y, int width, int height, boolean solid, Id id, Handler handler) {
		super(x, y, width, height, solid, id, handler);
	}

	/**
	 * Draws wall tile loading relative image.
	 * 
	 * @param Graphics g
	 */
	public void render(Graphics g) {
		g.drawImage(Game.getWll().getBufferedImage(), getX(), getY(), getWidth(), getHeight(), null);

	}

	public void tick() {
		// Not used as wall does nothing.
	}

}
