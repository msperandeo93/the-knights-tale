package tile;

import java.awt.Graphics;
import java.awt.Rectangle;

import knight.Handler;
import knight.Id;

public abstract class Tile {

	private int x;
	private int y;
	private int width;
	private int height;
	private boolean solid;

	private Id id;
	private Handler handler;

	public Tile(int x, int y, int width, int height, boolean solid, Id id, Handler handler) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.solid = solid;
		this.id = id;
		this.handler = handler;
	}

	public abstract void render(Graphics g);

	public abstract void tick();

	/**
	 * Allows removing of the tile.
	 * 
	 *
	 */
	public void die() {
		handler.removetile(this);
	}
	
	/**
	 * Returns X position.
	 * 
	 * @return this.x
	 */
	public int getX() {
		return this.x;
	}

	/**
	 * Returns Y position.
	 * 
	 * @return this.y
	 */
	public int getY() {
		return this.y;
	}
	
	public int getWidth(){
		return this.width;
	}
	
	public int getHeight(){
		return this.height;
	}
	
	/**
	 * Returns relative Id.
	 * 
	 * @return this.x
	 */
	public Id getId() {
		return this.id;
	}

	/**
	 * Defines whether an element is solid.
	 *
	 * @return solid
	 */
	public boolean isSolid() {
		return this.solid;
	}
	
	/**
	 * Returns a rectangle with same dimensions as actual tile.
	 * 
	 *
	 * @return Rectangle(x, y, width, height);
	 */
	public Rectangle getBounds() { // for collision detection
		return new Rectangle(x, y, width, height);
	}
}
