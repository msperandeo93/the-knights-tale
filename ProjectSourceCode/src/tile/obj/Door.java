package tile.obj;

import java.awt.Graphics;

import tile.Tile;
import knight.Game;
import knight.Handler;
import knight.Id;

public class Door extends Tile {

	public Door(int x, int y, int width, int height, boolean solid, Id id, Handler handler) {
		super(x, y, width, height, solid, id, handler);
	}

	/**
	 * Draws the door which allows jump to next level, loading relative image.
	 * 
	 * @param Graphics g
	 */
	public void render(Graphics g) {
		g.drawImage(Game.getDoor().getBufferedImage(), getX(), getY(), getWidth(), getHeight(), null);
	}

	public void tick() {
		//not used
	}

}
