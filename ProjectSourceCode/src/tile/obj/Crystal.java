package tile.obj;

import java.awt.Graphics;

import tile.Tile;
import knight.Game;
import knight.Handler;
import knight.Id;

public class Crystal extends Tile {

	public Crystal(int x, int y, int width, int height, boolean solid, Id id, Handler handler) {
		super(x, y, width, height, solid, id, handler);
	}

	/**
	 * Draws the crystal on given position, loading relative image.
	 * 
	 * @param Graphics g
	 * 
	 */
	public void render(Graphics g) {
		if (this.getId() == Id.crystal) {
			g.drawImage(Game.getCrst().getBufferedImage(), getX(), getY(), getWidth(), getHeight(), null);
		} else {
			g.drawImage(Game.getSpCrst().getBufferedImage(), getX(), getY(), getWidth(), getHeight(), null);
		}
	}

	public void tick() {
		//not used
	}

}
