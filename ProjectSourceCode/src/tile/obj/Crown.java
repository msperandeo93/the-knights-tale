package tile.obj;

import java.awt.Graphics;

import tile.Tile;
import knight.Game;
import knight.Handler;
import knight.Id;

public class Crown extends Tile {

	public Crown(int x, int y, int width, int height, boolean solid, Id id, Handler handler) {
		super(x, y, width, height, solid, id, handler);
	}

	/**
	 * Draws final crown which takes to victory, loading relative image.
	 *
	 * @param Graphics g
	 */
	public void render(Graphics g) {
		g.drawImage(Game.getCrwn().getBufferedImage(), getX(), getY(), getWidth(), getHeight(), null);
	}

	public void tick() {
		//not used
	}
}
