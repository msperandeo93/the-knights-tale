package entity.characters;

import java.awt.Graphics;
import java.util.Random;

import tile.Tile;
import entity.Entity;
import entity.EnergyWave;
import knight.Game;
import knight.Handler;
import knight.Id;

public class FirstBoss extends Entity {

	private Random random;
	private EnergyWave waveDx;
	private EnergyWave waveSx;

	public FirstBoss(int x, int y, int width, int height, boolean solid, Id id, Handler handler) {
		super(x, y, width, height, solid, id, handler);
		setLives(2);
		this.random = new Random();
		setDirection(random.nextInt(2));
		switch (getDir()) { //0=left; 1=right
		case 0:
			setVelX(-3);
			break;
		case 1:
			setVelX(3);
			break;
		}
	}

	/**
	 * Draws the monster loading relative image according to movement.
	 * 
	 *
	 * @param Graphics g
	 * 
	 */
	public void render(Graphics g) {
		if (getVelX() < 0) { // if it goes left
			g.drawImage(Game.getFrsBssSx().getBufferedImage(), getX(), getY(), getWidth(), getHeight(), null);
			if (getX() % (-3) == 0) {
				this.waveSx = new EnergyWave(getX()-70, getY(), 64, 64, true, Id.energyWaveBoss, getHandler());
				this.waveSx.setDirection(0);
				Game.getHandler().addEntity(this.waveSx);
			}
		} else {
			g.drawImage(Game.getFrsBssDx().getBufferedImage(), getX(), getY(), getWidth(), getHeight(), null);
			if (getX() % 3 == 0) {
				this.waveDx = new EnergyWave(getX()+70, getY(), 64, 64, true, Id.energyWaveBoss, getHandler());
				this.waveDx.setDirection(1);
				Game.getHandler().addEntity(this.waveDx);
			}
		}
	}

	/**
	 * Manages enemy in his game ambient, moving it left and right,
	 * the only limit is the wall, which changes his direction.
	 *  
	 * 
	 */
	public void tick() {
		setX(getX()+getVelX());
		setY(getY()+getVelY());
		for (int i = 0; i < getHandler().listTile().size(); i++) {
			Tile t = getHandler().listTile().get(i);
			if (t.isSolid()) {
				if (getBoundsBottom().intersects(t.getBounds())) {
					setVelY(0);
					if (isFalling())
						setFall(false);
				} else if (!isFalling()) {
					setFall(true);
					setGravity(0.8);
				}
				if (getBoundsLeft().intersects(t.getBounds())) {
					setVelX(3);
				}
				if (getBoundsRight().intersects(t.getBounds())) {
					setVelX(-3);
				}
			}
		}
		if (isFalling()) {
			setGravity(getGravity()+0.1);
			setVelY((int) getGravity());
		}
	}
}
