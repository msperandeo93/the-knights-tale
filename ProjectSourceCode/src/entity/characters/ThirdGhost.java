package entity.characters;

import java.awt.Graphics;
import java.util.Random;

import tile.Tile;
import entity.Entity;
import knight.Game;
import knight.Handler;
import knight.Id;

public class ThirdGhost extends Entity {

	private Random random;

	public ThirdGhost(int x, int y, int width, int height, boolean solid, Id id, Handler handler) {
		super(x, y, width, height, solid, id, handler);
		this.random = new Random();
		setDirection(this.random.nextInt(2));
		setLives(2);
		switch (getDir()) {
		case 0:
			setVelX(-4);
			break;
		case 1:
			setVelX(4);
			break;
		}
	}

	/**
	 * Draws the monster loading relative image according to movement.
	 *
	 * @param Graphics g
	 * 
	 */
	public void render(Graphics g) {
		if (getVelX() < 0) { // If goes left
			g.drawImage(Game.getThGhSx().getBufferedImage(), getX(), getY(), getWidth(), getHeight(), null);
		} else {
			g.drawImage(Game.getThGhDx().getBufferedImage(), getX(), getY(), getWidth(), getHeight(), null);
		}
	}

	/**
	 * Manages enemy in his game ambient, moving it left and right,
	 * the only limit is the wall, which changes his direction. 
	 * 
	 */
	public void tick() {
		setX(getX()+getVelX());
		setY(getY()+getVelY());
		for (int i = 0; i < getHandler().listTile().size(); i++) {
			Tile t = getHandler().listTile().get(i);
			if (t.isSolid()) {
				if (getBoundsBottom().intersects(t.getBounds())) {
					setVelY(0);
					if (isFalling())
						setFall(false);
				} else if (!isFalling()) {
					setFall(true);
					setGravity(0.8);
				}
				if (getBoundsLeft().intersects(t.getBounds())) {
					setVelX(4);
				}
				if (getBoundsRight().intersects(t.getBounds())) {
					setVelX(-4);
				}
			}
		}
		if (isFalling()) {
			setGravity(getGravity()+0.1);
			setVelY((int) getGravity());
		}
	}
}