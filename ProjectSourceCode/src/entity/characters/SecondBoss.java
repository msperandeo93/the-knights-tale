package entity.characters;

import java.awt.Graphics;
import java.util.Random;

import tile.Tile;
import entity.Entity;
import entity.EnergyWave;
import knight.Game;
import knight.Handler;
import knight.Id;

public class SecondBoss extends Entity {

	private Random random;
	private EnergyWave leftWaveDx;
	private EnergyWave leftWaveSx;
	private EnergyWave rightWaveDx;
	private EnergyWave rightWaveSx;

	public SecondBoss(int x, int y, int width, int height, boolean solid, Id id, Handler handler) {
		super(x, y, width, height, solid, id, handler);
		this.random = new Random();
		setDirection(random.nextInt(2));
		setLives(3);
		switch (getDir()) {
		case 0:
			setVelX(-2);
			break;
		case 1:
			setVelX(2);
			break;
		}
	}

	/**
	 * Draws the monster loading relative image according to movement.
	 *
	 * @param Graphics g
	 * 
	 */
	public void render(Graphics g) {
		if (getVelX() < 0) { // If goes left
			g.drawImage(Game.getScnBssSx().getBufferedImage(), getX(), getY(), getWidth(), getHeight(), null);
			if (getX()%(30)==0) {
				this.leftWaveSx = new EnergyWave(getX()-70, getY(), 64, 64, true, Id.energyWaveBoss, getHandler());
				this.leftWaveSx.setDirection(0);
				Game.getHandler().addEntity(this.leftWaveSx);
			}
			if (getX()%192==0) {
				this.leftWaveDx = new EnergyWave(getX()+70, getY(), 64, 64, true, Id.energyWaveBoss, getHandler());
				this.leftWaveDx.setDirection(1);
				Game.getHandler().addEntity(this.leftWaveDx);
			}
		} else { // If goes right
			g.drawImage(Game.getScnBssDx().getBufferedImage(), getX(), getY(), getWidth(), getHeight(), null);
			if (getX()%30 == 0) {
				this.rightWaveDx = new EnergyWave(getX()+70, getY(), 64, 64, true, Id.energyWaveBoss, getHandler());
				this.rightWaveDx.setDirection(1);
				Game.getHandler().addEntity(this.rightWaveDx);
			}
			if (getX()%192==0) {
				this.rightWaveSx = new EnergyWave(getX()-70, getY(), 64, 64, true, Id.energyWaveBoss, getHandler());
				this.rightWaveSx.setDirection(0);
				Game.getHandler().addEntity(this.rightWaveSx);
			}
		}
	}

	/**
	 * Manages enemy in his game ambient, moving it left and right,
	 * the only limit is the wall, which changes his direction. 
	 * 
	 */
	public void tick() {
		setX(getX()+getVelX());
		setY(getY()+getVelY());
		for (int i = 0; i < getHandler().listTile().size(); i++) {
			Tile t = getHandler().listTile().get(i);
			if (t.isSolid()) {
				if (getBoundsBottom().intersects(t.getBounds())) {
					setVelY(0);
					if (isFalling()){
						setFall(false);
					}
				} else if (!isFalling()) {
					setFall(true);
					setGravity(0.8);
				}
				if (getBoundsLeft().intersects(t.getBounds())) {
					setVelX(2);
				}
				if (getBoundsRight().intersects(t.getBounds())) {
					setVelX(-2);
				}
			}
		}
		if (isFalling()) {
			setGravity(getGravity()+0.1);
			setVelY((int) getGravity());
		}
	}
}