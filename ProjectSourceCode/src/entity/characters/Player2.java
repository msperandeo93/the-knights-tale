package entity.characters;

import java.awt.Graphics;

import entity.Entity;
import knight.Game;
import knight.Handler;
import knight.Id;
import tile.Tile;

//2nd Pg,  doesn't shoot, can jump on enemies
public class Player2 extends Entity{
		
	public Player2(int x, int y, int width, int height, boolean solid, Id id, Handler handler) {
		super(x, y, width, height, solid, id, handler);
	}

	/**
	 * According to my state(stop, jumping, moving), draws the knight loading right image.
	 * 
	 * @param Graphics g
	 * 
	 */
	public void render(Graphics g) {
		// Changes image when jumps or falls
		if ((getVelY() > 0 || getVelY() < 0) && (getVelX() >= 0)) {
			setDx(true);
			setSx(false);
			g.drawImage(Game.getPl2JmpD().getBufferedImage(), getX(), getY(), getWidth(), getHeight(), null);
		} else if ((getVelY() > 0 || getVelY() < 0) && (getVelX() < 0)) {
			setDx(false);
			setSx(true);
			g.drawImage(Game.getPl2JmpS().getBufferedImage(), getX(), getY(), getWidth(), getHeight(), null);
		} else if (getVelX() < 0) { // If goes left
			setSx(true);
			setDx(false);
			if ((getX() % (-8) == 0)) { // Changes image while moving left
				g.drawImage(Game.getPl2Sx1().getBufferedImage(), getX(), getY(), getWidth(), getHeight(), null);
			} else {
				g.drawImage(Game.getPl2Sx2().getBufferedImage(), getX(), getY(), getWidth(), getHeight(), null);
			} 
		} else if (getVelX() > 0) { // If goes right
			setSx(false);
			setDx(true);
			if ((getX() % (8) == 0)) { // Changes image while moving right
				g.drawImage(Game.getPl2Dx1().getBufferedImage(), getX(), getY(), getWidth(), getHeight(), null);
			} else {
				g.drawImage(Game.getPl2Dx2().getBufferedImage(), getX(), getY(), getWidth(), getHeight(), null);
			}
		} else if (getVelX() == 0) {
			if (isDx()) {
				g.drawImage(Game.getPl2Dx1().getBufferedImage(), getX(), getY(), getWidth(), getHeight(), null);
			} else if (isSx()) {
				g.drawImage(Game.getPl2Sx1().getBufferedImage(), getX(), getY(), getWidth(), getHeight(), null);
			}
		}
	}

	/**
	 * Manages the knight in his game ambient, managing his behaviors in all states it can be.
	 * For example: Death when he touches a monster.
	 * 
	 */
	public void tick() {
		setX(getX()+getVelX());
		setY(getY()+getVelY());
		for (int i = 0; i < getHandler().listTile().size(); i++) {
			Tile t = getHandler().listTile().get(i); // Manages tile types
			if (t.isSolid()) {
				if (getBoundsTop().intersects(t.getBounds())
						&& t.getId() == Id.wall) {
					setVelY(0);
					if (isJumping()) {
						setJump(false);
						setGravity(0.8);
						setFall(true);
					}
				}
				if (getBoundsBottom().intersects(t.getBounds())
						&& (t.getId() == Id.wall || t.getId() == Id.ground)) {
					setVelY(0);
					if (isFalling()){
						setFall(false);
					}
				} else if (!isFalling() && !isJumping()) {
					setFall(true);
					setGravity(0.8);
				}
				if (getBoundsLeft().intersects(t.getBounds())
						&& (t.getId() == Id.wall || t.getId() == Id.ground)) {
					setVelX(0);
					setX(t.getX() + t.getWidth());
				}
				if (getBoundsRight().intersects(t.getBounds())
						&& (t.getId() == Id.wall || t.getId() == Id.ground)) {
					setVelX(0);
					setX(t.getX() - t.getWidth());
				}
				if (getBounds().intersects(t.getBounds())
						&& (t.getId() == Id.crystal || t.getId() == Id.specialCystal)) {
					if (t.getId() == Id.crystal) {
						Game.setCrstlGet(1);
					} else {
						Game.setCrstlGet(5);
						Game.setLives(1);
					}
					Game.getObj().play();
					t.die();
				}

				if (getBounds().intersects(t.getBounds())
						&& (t.getId() == Id.door || t.getId() == Id.crown)) {
					if (t.getId() == Id.crown) {
						win();
						Game.getThmTn().stop();
					} else {
						Game.switchLevel();
					}
				}
			}
		}
		for (int i = 0; i < getHandler().listEntity().size(); i++) {
			Entity e = getHandler().listEntity().get(i); // Manages entity types

			if (e.getId() == Id.powerUp) {
				if (getBounds().intersects(e.getBounds())) {
					setPowUpGet(true);
					Game.getObj().play();
					e.die();
				}
			} else if ((e.getId() == Id.firstGhost)||(e.getId() == Id.secondGhost)||(e.getId() == Id.thirdGhost)
						||(e.getId() == Id.firstBoss)||(e.getId() == Id.secondBoss)
						||(e.getId() == Id.fllRock)||(e.getId() == Id.gun)) {
				if (getBoundsBottom().intersects(e.getBounds()) && 
						!getBoundsRight().intersects(e.getBounds()) && 
						!getBoundsLeft().intersects(e.getBounds())) {
					e.die();
					Game.getWvHit().play();
					switch(e.getId()){
					
					case firstGhost:
						Game.setFrstGhKll(1);
						break;
						
					case secondGhost:
						Game.getHandler().addEntity(new SecondGhost(e.getXorig(), e.getYorig(), 64, 64, true, Id.secondGhost, Game.getHandler()));
						break;
						
					case thirdGhost:
						Game.setThGhKll(1);
						break;
						
					case firstBoss:
						Game.setBoss1Kll(1);
						Game.getHandler().addTile(new tile.obj.Crystal(e.getX(), e.getY(), 64, 64, true, Id.specialCystal, getHandler()));
						break;
						
					case secondBoss:
						Game.setBoss2Kll(1);
						Game.getHandler().addTile(new tile.obj.Crystal(e.getX(), e.getY(), 64, 64, true, Id.specialCystal, getHandler()));
						break;
					
					default:
						break;
							
					}
					
					
				} else if(getBounds().intersects(e.getBounds())){
					die();
				}
			} else if (e.getId() == Id.energyWave) {
				e.die();
			}
		}
		if (isJumping()) {
			setGravity(getGravity()-0.17);
			setVelY((int)-getGravity());
			if (getGravity() <= 0.5) {
				setJump(false);
				setFall(true);
			}
		}
		if (isFalling()) {
			setGravity(getGravity()+0.17);
			setVelY((int) getGravity());
		}
	}
}
