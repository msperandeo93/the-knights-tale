package entity.characters;

import java.awt.Graphics;

import tile.Tile;
import entity.Entity;
import knight.Game;
import knight.Handler;
import knight.Id;

public class FallingRock extends Entity {
	
	public FallingRock(int x, int y, int width, int height, boolean solid, Id id, Handler handler) {
		super(x, y, width, height, solid, id, handler);
		
	}
	
	/**
	 * Draws the monster loading relative image, according to movement
	 *
	 * @param Graphics g
	 * 
	 */
	public void render(Graphics g) {
		g.drawImage(Game.getFllRock().getBufferedImage(), getX(), getY(), getWidth(), getHeight(), null);
	}

	/**
	 * Manages enemy in his game ambient, moving it left and right,
	 * the only limit is the wall, which changes his direction. 
	 * 
	 */
	public void tick() {
		setX(getX()+getVelX());
		setY(getY()+getVelY());
		for(int i=0; i<getHandler().listTile().size(); i++){
			Tile t = getHandler().listTile().get(i);
			if(t.isSolid()){
				if(getBoundsBottom().intersects(t.getBounds())){
					die();
					Game.getHandler().addEntity(new FallingRock(getXorig(), getYorig(), 64, 64, true, Id.fllRock, Game.getHandler()));
 				}else if(!isFalling()){
 					setFall(true);
 					setGravity(0.8);
 				}
			}
		}
		if(isFalling()){
			setGravity(getGravity()+0.1);
			setVelY((int)getGravity());
		}
	}
}