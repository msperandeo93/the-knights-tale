package entity.characters;

import java.awt.Graphics;

import tile.Tile;
import entity.EnergyWave;
import entity.Entity;
import knight.Game;
import knight.Handler;
import knight.Id;

public class Gun extends Entity {

	private boolean flak = true;
	
	public Gun(int x, int y, int width, int height, boolean solid, Id id, Handler handler) {
		super(x, y, width, height, solid, id, handler);
		
	}
	
	/**
	 * Draws the monster loading relative image, according to movement
	 *
	 * @param Graphics g
	 * 
	 */
	public void render(Graphics g) {
		g.drawImage(Game.getGun().getBufferedImage(), getX(), getY(), getWidth(), getHeight(), null);
	}

	/**
	 * Manages enemy in his game ambient, moving it left and right,
	 * the only limit is the wall, which changes his direction. 
	 * 
	 */
	public void tick() {
		setX(getX()+getVelX());
		setY(getY()+getVelY());
		for (int i = 0; i < getHandler().listTile().size(); i++) {
			Tile t = getHandler().listTile().get(i); // Manages tile types
			if (t.isSolid()) {
				if (getBoundsTop().intersects(t.getBounds())
						&& t.getId() == Id.wall) {
					setVelY(0);
					if (isJumping()) {
						setJump(false);
						setGravity(0.8);
						setFall(true);
					}
				}
				if (getBoundsBottom().intersects(t.getBounds())) {
					if (!isFire()){
						setFire(true);
						Entity waveSx = new EnergyWave(getX() - 70, getY(), 64, 64, true, Id.slug, Game.getHandler());
						waveSx.setDirection(0);
						if(flak){
							Game.getHandler().addEntity(waveSx);
						}
						setFire(false);
						if(flak){
							flak=false;
						}else{
							flak=true;
						}
					}
					if(!isJumping()){
						setJump(true);
						setGravity(8.0);
					}
					if (isFalling()){
						setFall(false);
					}
				} else if (!isFalling() && !isJumping()) {
					setFall(true);
					setGravity(0.8);
				}
			}
		}
		if (isJumping()) {
			setGravity(getGravity()-0.17);
			setVelY((int)-getGravity());
			if (getGravity() <= 0.5) {
				setJump(false);
				setFall(true);
			}
		}
		if (isFalling()) {
			setGravity(getGravity()+0.17);
			setVelY((int) getGravity());
		}
	}
}