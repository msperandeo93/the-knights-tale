package entity.characters;

import java.awt.Graphics;
import java.util.Random;

import tile.Tile;
import entity.Entity;
import knight.Game;
import knight.Handler;
import knight.Id;

public class SecondGhost extends Entity {

	private Random random;

	public SecondGhost(int x, int y, int width, int height, boolean solid, Id id, Handler handler) {
		super(x, y, width, height, solid, id, handler);
		this.random = new Random();
		setDirection(random.nextInt(2));
		switch (getDir()) {
		case 0:
			setVelX(-3);
			break;
		case 1:
			setVelX(3);
			break;
		}
	}

	/**
	 * Draws the monster loading relative image according to movement.
	 *
	 * @param Graphics g
	 * 
	 */
	public void render(Graphics g) {
		if (getVelX() < 0) { // If goes left
			g.drawImage(Game.getScnGhSx().getBufferedImage(), getX(), getY(), getWidth(), getHeight(), null);
		} else {
			g.drawImage(Game.getScnGhDx().getBufferedImage(), getX(), getY(), getWidth(), getHeight(), null);
		}
	}

	/**
	 *
	 * Manages enemy in his game ambient, moving it left and right,
	 * the only limit is the wall, which changes his direction. 
	 * 
	 */
	public void tick() {
		setX(getX()+getVelX());
		setY(getY()+getVelY());
		for (int i = 0; i < getHandler().listTile().size(); i++) {
			Tile t = getHandler().listTile().get(i);
			if (t.isSolid()) {
				if (getBoundsBottom().intersects(t.getBounds())) {
					setVelY(0);
					if (isFalling()){
						setFall(false);
					}
				} else if (!isFalling()) {
					setFall(true);
					setGravity(0.8);
				}
				if (getBoundsLeft().intersects(t.getBounds())) {
					setVelX(3);
				}
				if (getBoundsRight().intersects(t.getBounds())) {
					setVelX(-3);
				}
			}
		}
		if (isFalling()) {
			setGravity(getGravity()+0.1);
			setVelY((int) getGravity());
		}
	}
}