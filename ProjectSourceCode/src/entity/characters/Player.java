package entity.characters;

import java.awt.Graphics;

import tile.Tile;
import entity.Entity;
import knight.Game;
import knight.Handler;
import knight.Id;

public class Player extends Entity {

	public Player(int x, int y, int width, int height, boolean solid, Id id, Handler handler) {
		super(x, y, width, height, solid, id, handler);
	}

	/**
	 * According to my state(stop, jumping, moving), draws the knight loading right image.
	 * 
	 * @param Graphics g
	 * 
	 */
	public void render(Graphics g) {
		// Changes image when jumps or falls
		if ((getVelY() > 0 || getVelY() < 0) && (getVelX() >= 0)) {
			setDx(true);
			setSx(false);
			g.drawImage(Game.getPlJmpDx().getBufferedImage(), getX(), getY(), getWidth(), getHeight(), null);
		} else if ((getVelY() > 0 || getVelY() < 0) && (getVelX() < 0)) {
			setDx(false);
			setSx(true);
			g.drawImage(Game.getPlJmpSx().getBufferedImage(), getX(), getY(), getWidth(), getHeight(), null);
		} else if (getVelX() < 0) { // If goes left
			setSx(true);
			setDx(false);
			if ((getX() % (-8) == 0)) { // Changes image while moving left
				g.drawImage(Game.getPlSx1().getBufferedImage(), getX(), getY(), getWidth(), getHeight(), null);
			} else {
				g.drawImage(Game.getPlSx2().getBufferedImage(), getX(), getY(), getWidth(), getHeight(), null);
			}
		} else if (isFire()) { // Changes image when shooting
			if (isDx()) {
				g.drawImage(Game.getPlFrDx().getBufferedImage(), getX(), getY(), getWidth(), getHeight(), null);
			} else if (isSx()) {
				g.drawImage(Game.getPlFrSx().getBufferedImage(), getX(), getY(), getWidth(), getHeight(), null);
			}
		} else if (getVelX() > 0) { // If goes right
			setSx(false);
			setDx(true);
			if ((getX() % (8) == 0)) { // Changes image while moving right
				g.drawImage(Game.getPlDx1().getBufferedImage(), getX(), getY(), getWidth(), getHeight(), null);
			} else {
				g.drawImage(Game.getPlDx2().getBufferedImage(), getX(), getY(), getWidth(), getHeight(), null);
			}
		} else if (getVelX() == 0) {
			if (isDx()) {
				g.drawImage(Game.getPlDx1().getBufferedImage(), getX(), getY(), getWidth(), getHeight(), null);
			} else if (isSx()) {
				g.drawImage(Game.getPlSx1().getBufferedImage(), getX(), getY(), getWidth(), getHeight(), null);
			}
		}
	}

	/**
	 * Manages the knight in his game ambient, managing his behaviors in all states it can be.
	 * For example: Death when he touches a monster.
	 * 
	 */
	public void tick() {
		setX(getX()+getVelX());
		setY(getY()+getVelY());
		for (int i = 0; i < getHandler().listTile().size(); i++) {
			Tile t = getHandler().listTile().get(i); // Manages tile types
			if (t.isSolid()) {
				if (getBoundsTop().intersects(t.getBounds())
						&& t.getId() == Id.wall) {
					setVelY(0);
					if (isJumping()) {
						setJump(false);
						setGravity(0.8);
						setFall(true);
					}
				}
				if (getBoundsBottom().intersects(t.getBounds())
						&& (t.getId() == Id.wall || t.getId() == Id.ground)) {
					setVelY(0);
					if (isFalling()){
						setFall(false);
					}
				} else if (!isFalling() && !isJumping()) {
					setFall(true);
					setGravity(0.8);
				}
				if (getBoundsLeft().intersects(t.getBounds())
						&& (t.getId() == Id.wall || t.getId() == Id.ground)) {
					setVelX(0);
					setX(t.getX() + t.getWidth());
				}
				if (getBoundsRight().intersects(t.getBounds())
						&& (t.getId() == Id.wall || t.getId() == Id.ground)) {
					setVelX(0);
					setX(t.getX() - t.getWidth());
				}
				if (getBounds().intersects(t.getBounds())
						&& (t.getId() == Id.crystal || t.getId() == Id.specialCystal)) {
					if (t.getId() == Id.crystal) {
						Game.setCrstlGet(1);
					} else {
						Game.setCrstlGet(5);
						Game.setLives(1);
					}
					Game.getObj().play();
					t.die();
				}

				if (getBounds().intersects(t.getBounds())
						&& (t.getId() == Id.door || t.getId() == Id.crown)) {
					if (t.getId() == Id.crown) {
						win();
						Game.getThmTn().stop();
					} else {
						Game.switchLevel();
					}
				}
			}
		}
		for (int i = 0; i < getHandler().listEntity().size(); i++) {
			Entity e = getHandler().listEntity().get(i); // Manages entity types

			if (e.getId() == Id.powerUp) {
				if (getBounds().intersects(e.getBounds())) {
					setPowUpGet(true);
					Game.getObj().play();
					e.die();
				}
			} else if ((e.getId() == Id.firstGhost)||(e.getId() == Id.secondGhost)||(e.getId() == Id.thirdGhost)
						||(e.getId() == Id.firstBoss)||(e.getId() == Id.secondBoss)
						||(e.getId() == Id.fllRock)||(e.getId() == Id.gun)) {
				if (getBounds().intersects(e.getBounds())) {
					Game.getPlyrHit().play();
					die();
				} else if (e.getId() == Id.energyWave) {
					e.die();
				}
			}
		}
		if (isJumping()) {
			setGravity(getGravity()-0.17);
			setVelY((int)-getGravity());
			if (getGravity() <= 0.5) {
				setJump(false);
				setFall(true);
			}
		}
		if (isFalling()) {
			setGravity(getGravity()+0.17);
			setVelY((int) getGravity());
		}
	}
}