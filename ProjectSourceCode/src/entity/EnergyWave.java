package entity;

import java.awt.Graphics;

import tile.Tile;
import entity.characters.*;
import knight.Game;
import knight.Handler;
import knight.Id;

public class EnergyWave extends Entity {

	public EnergyWave(int x, int y, int width, int height, boolean solid, Id id, Handler handler) {
		super(x, y, width, height, solid, id, handler);
	}

	/**
	 * Draws the attack, loading relative image.
	 *
	 * @param Graphics g
	 */
	public void render(Graphics g) {
		if (getId() == Id.energyWaveBoss) {
			if (getDir() == 0) {
				g.drawImage(Game.getEnrgWvBssSx().getBufferedImage(), getX(), getY(), getWidth(), getHeight(), null);
			} else {
				g.drawImage(Game.getEnrgWvBssDx().getBufferedImage(), getX(), getY(), getWidth(), getHeight(), null);
			}
		} else if(getId() == Id.slug) {
			g.drawImage(Game.getSlug().getBufferedImage(), getX(), getY(), getWidth(), getHeight(), null);
		} else {
			g.drawImage(Game.getEnrgWv().getBufferedImage(), getX(), getY(), getWidth(), getHeight(), null);
		}
	}

	/**
	 * Manages the attack in game ambient, moving it horizontally.
	 * 
	 *
	 */
	public void tick() {
		setX(getX()+getVelX());
		setY(getY()+getVelY());
		//x += velX;
		//y += velY;
		for (int i = 0; i < getHandler().listTile().size(); i++) {
			Tile t = getHandler().listTile().get(i);
			if (t.isSolid()) {
				if (getBoundsBottom().intersects(t.getBounds())) {
					setVelY(0);
					if (isFalling()){
						//falling = false;
						setFall(false);
					}
				} else if (!isFalling()) {
					setFall(true);
					setGravity(0.8);
//					falling = true;
//					gravity = 0.8;
				}
				if (getBoundsLeft().intersects(t.getBounds())
						&& (t.getId() == Id.wall || t.getId() == Id.ground)) {
					die();
					Game.getWvHit().play();
				}
				if (getBoundsRight().intersects(t.getBounds())) {
					die();
					Game.getWvHit().play();
				}
				if (getBounds().intersects(t.getBounds())
						&& t.getId() == Id.crystal) {
					die();
					Game.getWvHit().play();
				}
			}
		}
		for (int i = 0; i < getHandler().listEntity().size(); i++) {
			Entity e = getHandler().listEntity().get(i); // manages entity tipes.
			if (e.getId() == Id.powerUp) {
				if (getBounds().intersects(e.getBounds())) {
					die();
					Game.getWvHit().play();
				}
			} else if (e.getId() == Id.firstGhost) { // mob 1
				if (getBounds().intersects(e.getBounds())) {
					e.die();
					die();
					Game.setFrstGhKll(1);
					Game.getExpld().play();
				}
			} else if (e.getId() == Id.secondGhost) { // mob 2 orange, respawns.
				if (getBounds().intersects(e.getBounds())) {
					e.die();
					die();
					Game.getExpld().play();
					Game.getHandler().addEntity(new SecondGhost(e.getXorig(), e.getYorig(), 64, 64, true, Id.secondGhost, Game.getHandler()));
				}
			} else if (e.getId() == Id.thirdGhost) { // mob bordeaux, has 2 lives.
				if (getBounds().intersects(e.getBounds())) {
					if (e.getLives() > 1) {
						e.hit();
						Game.getWvHit().play();
					} else {
						e.die();
						Game.getExpld().play();
						Game.setThGhKll(1);
					}
					die();
				}
			} else if (e.getId() == Id.fllRock) { 
					if (getBounds().intersects(e.getBounds())) {
						e.die();
						die();
						Game.getHandler().addEntity(new FallingRock(e.getXorig(), e.getYorig(), 64, 64, true, Id.fllRock, Game.getHandler()));
						Game.getExpld().play();
					}
			} else if (e.getId() == Id.gun) { 
				if (getBounds().intersects(e.getBounds())) {
					e.die();
					die();
					e.setFire(false);
					Game.getExpld().play();
				}
			} else if (e.getId() == Id.firstBoss) {
				if (getBounds().intersects(e.getBounds())) {
					if (e.getLives() > 1) {
						e.hit();
						Game.getExpld().play();
					} else {
						e.die();
						Game.setBoss1Kll(1);
						Game.getExpld().play();
						Game.getHandler().addTile(new tile.obj.Crystal(e.getX(), e.getY(), 64, 64, true, Id.specialCystal, getHandler()));
					}
					die();
				}
			} else if (e.getId() == Id.secondBoss) {
				if (getBounds().intersects(e.getBounds())) {
					if (e.getLives() > 1) {
						e.hit();
						Game.getExpld().play();
					} else {
						e.die();
						Game.setBoss2Kll(1);
						Game.getExpld().play();
						Game.getHandler().addTile(new tile.obj.Crystal(e.getX(), e.getY(), 64, 64, true, Id.specialCystal, getHandler()));
					}
					die();
				}
			} else if (e.getId() == Id.player) {
				if (getBounds().intersects(e.getBounds())) {
					die();
					e.die();
					Game.getPlyrHit().play();
				}
			} else if (e.getId() == Id.energyWaveBoss) {
				if (getBounds().intersects(e.getBounds())) {
					e.die();
					die();
					Game.getWvHit().play();
				}
			} else if(e.getId() == Id.slug){
				if (getBoundsRight().intersects(e.getBoundsLeft())) {
					e.die();
					die();
					Game.getWvHit().play();
				}
			}
		}
		if (isFalling()) {
			setGravity(getGravity()+0.1);
			setVelY((int) getGravity());
		}
	}
}
