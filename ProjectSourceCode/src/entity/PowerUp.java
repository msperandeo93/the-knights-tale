package entity;

import java.awt.Graphics;

import entity.Entity;
import knight.Game;
import knight.Handler;
import knight.Id;

public class PowerUp extends Entity {

	public PowerUp(int x, int y, int width, int height, boolean solid, Id id, Handler handler) {
		super(x, y, width, height, solid, id, handler);
	}

	/**
	 * Draws the power up, loading the specified image.
	 *
	 * @param Graphics g
	 * 
	 */
	public void render(Graphics g) {
		g.drawImage(Game.getPwUp().getBufferedImage(), getX(), getY(), getWidth(), getHeight(), null);
	}

	public void tick() {
		//not used
	}

}
