package entity;

import java.awt.Graphics;
import java.awt.Rectangle;

import knight.Game;
import knight.Handler;
import knight.Id;

public abstract class Entity {

	private int x;
	private int y;
	private int dir;
	private int width;
	private int height;
	private double gravity;
	private int velX;
	private int velY;
	private int lives;
	private int xOrig; 
	private int yOrig;
	private boolean solid;
	private boolean powerUpGet = false;
	private boolean jumping = false;
	private boolean falling = true;
	private boolean fire = false;
	private boolean DX = true;
	private boolean SX = false;	

	private Id id;
	private Handler handler;

	public Entity(int x, int y, int width, int height, boolean solid, Id id, Handler handler) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.solid = solid;
		this.id = id;
		this.handler = handler;
		setxOrig(x);
		setyOrig(y);
	}

	public abstract void render(Graphics g);

	public abstract void tick();

	/**
	 * Manages players' deaths counting remaining lives.
	 * During die or gameover event, calls sound event methods 
	 * and draws Deathscreen or GameOver screens.
	 * 
	 *
	 */
	public void die() {
		handler.removeEntity(this);
		
		if (getId() == Id.player) {
			Game.setLives(-1);
			handler.setTotCry(true); // Reset total of objects created within the level.
			handler.setTotFirstGh(true);
			handler.setTotSecGh(true);
			handler.setTotThrdGh(true);
			Game.setCrstlGet(0); // Reset total of picked up objects and enemies killed.
			Game.setFrstGhKll(0); 
			Game.setThGhKll(0);
			Game.setDthScrn(true);
			Game.setBgnGm(false);
			Game.getPlyrHit().play();
			Game.getThmTn().stop();
			
			if (Game.getLives() == 0) {
				Game.getGmOvTn().play();
				Game.getChrn().stopTime();
				Game.setTotPoints((Game.getTotCrstlGet())+(Game.getTotGhKll())+
						(Game.getBoss1Kll())+(Game.getBoss2Kll()));
				Game.getRnk().getMap().put((int) Game.getTotPoints(), Game.getNm());
				Game.getRnk().write(Game.getRnk().getMap());
				Game.setGmOvr(true);
			}
		}
	}

	/**
	 * Manages player win, calling relative sound method 
	 * and creating win screen.
	 * 
	 */
	public void win() {
		Game.getChrn().stopTime();
		Game.setTotPoints((((Game.getTotCrstlGet()*3)+(Game.getTotGhKll()*2)+(Game.getBoss1Kll()*4)+(Game.getBoss2Kll()*5)) 
				*Game.getChrn().getTimeScore())*2);
		handler.removeEntity(this);
		if (getId() == Id.player) {
			Game.setDthScrn(true);
			Game.setGmOvr(true);
			Game.setWnnr(true);
			Game.getThmTn().stop();
			Game.getWnnTn().play();
			Game.getRnk().getMap().put((int) Game.getTotPoints(), Game.getNm());
			Game.getRnk().write(Game.getRnk().getMap());
		}
	}

	public void setX(int x){
		this.x = x;
	}
	
	public void setY(int y){
		this.y = y;
	}
	
	/**
	 * Saves X value when entity is created.
	 *
	 * @param
	 */
	private final void setxOrig(int xOrig) {
		this.xOrig = xOrig;
	}

	/**
	 * Saves Y value when entity is created.
	 *
	 * @param
	 */
	private final void setyOrig(int yOrig) {
		this.yOrig = yOrig;
	}
	
	/**
	 * Sets horizontal speed's value.
	 *
	 * @param velX
	 */
	public void setVelX(int velX) {
		this.velX = velX;
	}

	/**
	 * Sets vertical speed's value.
	 *
	 * @param velY
	 */
	public void setVelY(int velY) {
		this.velY = velY;
	}

	/**
	 * Sets number of lives.
	 *
	 * @param velX
	 */
	public void setLives(int nLives) {
		this.lives = nLives;
	}
	
	/**
	 * Decreases the number of lives
	 *
	 */
	public void hit() { 
		this.lives--;
	}
	
	/**
	 * Sets the direction of the Entity
	 *
	 * @param dir
	 */
	public void setDirection(int dir){
		if(dir == 0){
			this.dir = 0;
			setVelX(-5);
		} else {
			this.dir = 1;
			setVelX(5);
		}
	}

	public void setGravity(double gravity){
		this.gravity = gravity;
	}
	
	public void setFall(boolean fall){
		this.falling = fall;
	}
	
	public void setJump(boolean jump){
		this.jumping = jump;
	}
	
	public void setPowUpGet(boolean powUpGet){
		this.powerUpGet = powUpGet;
	}
	
	public void setDx(boolean dirDx){
		this.DX = dirDx;
	}
	
	public void setSx(boolean dirSx){
		this.SX = dirSx;
	}
	
	public void setFire(boolean fire){
		this.fire = fire;
	}
	
	/**
	 * Returns the number of lives.
	 * 
	 * @return lives
	 */
	public int getLives() {
		return lives;
	}

	/**
	 * Returns X position.
	 * 
	 * @return this.x
	 */
	public int getX() {
		return this.x;
	}

	/**
	 * Retursn Y position.
	 *
	 * @return this.y
	 */
	public int getY() {
		return this.y;
	}

	public int getDir(){
		return this.dir;
	}
	
	public int getHeight(){
		return this.height;
	}
	
	public int getWidth(){
		return this.width;
	}
	
	/**
	 * Returns original X position.
	 *
	 * @return this.y
	 */
	public int getXorig() {
		return this.xOrig;
	}

	/**
	 * Returns original Y position.
	 *
	 * @return this.y
	 */
	public int getYorig() {
		return this.yOrig;
	}

	/**
	 * Returns X speed and direction.
	 * 
	 * @return this.x
	 */
	public int getVelX() {
		return this.velX;
	}

	/**
	 * Returns Y speed and direction.
	 * 
	 * @return this.x
	 */
	public int getVelY() {
		return this.velY;
	}

	/**
	 * Return this entity's Id.
	 *
	 * @return id
	 */
	public Id getId() {
		return this.id;
	}
	
	public Handler getHandler(){
		return this.handler;
	}
	
	public double getGravity(){
		return this.gravity;
	}
	
	/**
	 * Tells whether an element is solid.
	 *
	 * @return solid
	 */
	public boolean isSolid() {
		return this.solid;
	}
	
	public boolean isPowerUpGet(){
		return this.powerUpGet;
	}
	
	public boolean isJumping(){
		return this.jumping;
	}
	
	public boolean isFalling(){
		return this.falling;
	}
	
	public boolean isFire(){
		return this.fire;
	}
	
	public boolean isDx(){
		return this.DX;
	}
	
	public boolean isSx(){
		return this.SX;
	}

	/**
	 * Returns a rectangle for collision detection.
	 *
	 * @return Rectangle(x, y, width, heigh)
	 */
	public Rectangle getBounds() { // for collision detection
		return new Rectangle(x, y, width, height);
	}

	/**
	 * Returns a rectangle reporting dimensions of superior edge of specified entities.
	 * collision detection
	 *
	 * @return Rectangle(getX()+10,getY(),width-20,5)
	 */
	public Rectangle getBoundsTop() {
		return new Rectangle(getX() + 10, getY(), width - 20, 5);
	}

	/**
	 * Returns a rectangle reporting dimensions of inferior edge of specified entities.
	 * collision detection
	 *
	 * @return Rectangle(getX()+10,getY()+height-5,width-20,5);
	 */
	public Rectangle getBoundsBottom() {
		return new Rectangle(getX() + 10, getY() + height - 5, width - 20, 5);
	}

	/**
	 * Returns a rectangle reporting dimensions of left edge of specified entities.
	 * collision detection
	 *
	 * @return Rectangle(getX(),getY()+10,5,height-20);
	 */
	public Rectangle getBoundsLeft() {
		return new Rectangle(getX(), getY() + 10, 5, height - 20);
	}

	/**
	 * Returns a rectangle reporting dimensions of right edge of specified entities.
	 * collision detection
	 *
	 * @return Rectangle(getX()+width-5,getY()+10,5,height-20);
	 */
	public Rectangle getBoundsRight() {
		return new Rectangle(getX() + width - 5, getY() + 10, 5, height - 20);
	}
}